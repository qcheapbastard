#ifndef QCHEAPBASTARD_H
#define QCHEAPBASTARD_H

#include <QtGui>
#include <QtNetwork>
#include <qfile.h>

class QCheapBastard : public QWidget
{
    Q_OBJECT

public:
    QCheapBastard(QWidget *p = 0);

private slots:
    void findDeals();
    void viewDoubleClicked(QModelIndex);
private:
    void parseResults();
    void addResults(QList<QStandardItem *>);
    void popNextOffer();

    QSystemTrayIcon *trayIcon;
    QHttp *http;
    QFile *file;
    int getId;
    QListView *view;
    QStandardItemModel *model;
    QList<QStandardItem*> *list;
    QTimer *timer;
    QMenu *menu;
    QQueue<QStandardItem*> *queue;
    QStandardItem *currentOffer;

private slots:
    void httpRequestFinished( int, bool );
    void balloonClicked();
    void activated( QSystemTrayIcon::ActivationReason );
};
#endif

