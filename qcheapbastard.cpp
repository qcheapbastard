#include "qcheapbastard.h"

#include <QtNetwork>
#include <qfile.h>

QCheapBastard::QCheapBastard( QWidget *p )
    : QWidget (p) {

    resize(400,600);
    setWindowTitle( "QCheapBastard" );
    queue = new QQueue<QStandardItem*>();
    model = new QStandardItemModel();

    view = new QListView( this );
    view->setModel( model );
    connect( view,SIGNAL( doubleClicked( const QModelIndex & ) ),
            this, SLOT( viewDoubleClicked( QModelIndex ) ) );

    QPushButton *pb = new QPushButton( "Update!" );
    connect( pb, SIGNAL( clicked() ),this, SLOT( findDeals() ) );

    QGridLayout *layout = new QGridLayout;
    layout->addWidget( pb, 0, 0 );
    layout->addWidget( view, 1, 0 );
    setLayout(layout);

    menu = new QMenu( this );
    menu->addAction("&Quit", qApp, SLOT(quit()));

    trayIcon = new QSystemTrayIcon( QIcon( "icon.png" ), this );
    connect( trayIcon, SIGNAL( messageClicked() ),
            this, SLOT( balloonClicked() ) );
    connect( trayIcon, SIGNAL( activated( QSystemTrayIcon::ActivationReason ) ),
            this, SLOT( activated( QSystemTrayIcon::ActivationReason ) ) );
    trayIcon->setContextMenu( menu );
    trayIcon->show();

    timer = new QTimer(this);
    connect( timer, SIGNAL( timeout() ), this, SLOT( findDeals() ) );
    timer->start( 60000 );
}

void QCheapBastard::balloonClicked(){
    qDebug() << "Trying to open browser to visit" << currentOffer->toolTip();
    QDesktopServices::openUrl( QUrl( currentOffer->toolTip() ) );

    popNextOffer();
}

void QCheapBastard::activated( QSystemTrayIcon::ActivationReason reason ) {

    if ( reason == QSystemTrayIcon::DoubleClick )
        this->setVisible( !this->isVisible() );
    if ( reason == QSystemTrayIcon::Trigger )
        popNextOffer();
}

void QCheapBastard::popNextOffer(){

    if ( queue->isEmpty() ) {
        trayIcon->showMessage( "", "", QSystemTrayIcon::Warning, 1 );
        return;
    }

    currentOffer = queue->dequeue();
    trayIcon->showMessage( "More free crap!", currentOffer->text(), QSystemTrayIcon::Information, 100000000 );

}

void QCheapBastard::viewDoubleClicked( QModelIndex idx ){
    QDesktopServices::openUrl( QUrl( idx.data(Qt::ToolTipRole).toString() ) );
}

void QCheapBastard::findDeals(){
    http = new QHttp( this );
    file = new QFile( "get.html" );

    connect( http, SIGNAL( requestFinished( int, bool ) )
            ,this, SLOT( httpRequestFinished( int, bool ) ) );

    if ( !file->open( QIODevice::WriteOnly ) ){
        qDebug() << "file open broke!";
        return;
    }
    http->setHost( "www.finn.no" );
    getId = http->get( "/finn/bap/all/result?channelId=1&SEGMENT=1%3B2&userid=0&TYPE=1&areaId=20061&searchKey=SEARCH_ID_BAP_ALL&siteId=1&CATEGORY/MAINCATEGORY=93", file );
    //getId = http->get( "/finn/bap/free/result?TYPE=2&SEGMENT=1&rows=10", file );
}

void QCheapBastard::httpRequestFinished( int id, bool error ){

    if ( error )
        qDebug() << "httpRequestFinished reports that an error occured";

    if ( id == getId ) {
        file->close();
        parseResults();
    }
}

void QCheapBastard::parseResults(){

    if ( !file->open( QIODevice::ReadOnly ) ){
        qDebug() << "file open broke in parser!";
        return;
    }

    QByteArray htmlData = file->readAll();

    delete file;
//   delete http;

    QRegExp rx( "<a href=\"(/finn/bap/object\\\?finnkode=.{0,45}tot=[\\d]{0,6})\">([^<].*)</a>" );
    rx.setMinimal( TRUE );
    rx.setPatternSyntax( QRegExp::RegExp2 );

    QStringList newResults;
    QList<QStandardItem*> newList;
    int pos = 0;
    while ( ( pos = rx.indexIn( htmlData, pos ) ) != -1 ) {
        QStandardItem *item = new QStandardItem( rx.cap( 2 ) );
        item->setToolTip( "http://www.finn.no" + rx.cap( 1 ) );
        item->setEditable( FALSE );
        newList << item;
        pos += rx.matchedLength();
    }

    addResults( newList );

    view->repaint();
}

void QCheapBastard::addResults(QList<QStandardItem *> newResults){
    for( int i=0; i < newResults.size(); ++i ){

        if ( model->findItems( newResults[i]->text(), Qt::MatchExactly ).isEmpty() ){
            model->appendRow( newResults[i] );
            queue->enqueue( newResults[i] );
        }
    }
    popNextOffer();
}
